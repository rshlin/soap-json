#SOAP-JSON example
Реализация веб сервиса, работающего с json payload'ом. Подробнее см. TASK.md

####Детали реализации:
- в отсутствие схемы таблиц с отделами и должностями реализованы справочники на слое маппинга pojo
````java
@Mapper
public interface OperatorMapper {
    OperatorMapper INSTANCE = Mappers.getMapper(OperatorMapper.class);

    @Mappings({
            @Mapping(source = "idOperator", target = "id"),
            @Mapping(source = "family", target = "surname"),
            @Mapping(source = "farth", target = "patronymic"),
            @Mapping(expression = "java(io.rshlin.soap_json.ws.dto.OperatorMapper.BRANCH_NAMES.getOrDefault(entity.getIdOtdel(), \"Неизвестно\"))", target = "branch"),
            @Mapping(expression = "java(io.rshlin.soap_json.ws.dto.OperatorMapper.POSITION_NAMES.getOrDefault(entity.getPosition(), \"Неизвестно\"))", target = "position")
    })
    OperatorDto entityToDto(OperatorEntity entity);

    Map<Integer, String> BRANCH_NAMES = new HashMap<Integer, String>(){{
        put(186, "Уполномоченный МФЦ");
    }};
    Map<Short, String> POSITION_NAMES = new HashMap<Short, String>(){{
        put((short) 0, "Оператор");
    }};
}
````
- в отсутствие информации об уникальности значений колонок API отдает результат в array, пример:
``````JSON
{
  "success":true,
  "description":"Операция выполнена успешно",
  "operators":[
    {
      "id":"2222",
      "surname":"Foo",
      "name":"Bar",
      "patronymic":"Foobar",
      "position":"Оператор",
      "branch":"Уполномоченный МФЦ"
      }
    ]
}
``````
- в запросе может быть несколько полей. Пример запроса :
```JSON
{
    "snils": "108-003-636 06",
    "idOperator" : 2222
}
```
Решение готово для быстрой модификации API для сравнений like, gt, lt: на бэкенде используется JPA criteria API
##Настройка сервиса
Для интеграции с БД необходимо отредактировать значения файла src/main/resources/application.properties:
````properties
spring.datasource.url=jdbc:sqlserver://localhost:1433;database=YOURDB
spring.datasource.username=USERNAME
spring.datasource.password=PASSWORD
````
Для внесения тестовых данных в бд необходимо отредактировать атрибут:
````properties
io.rshlin.testData=init
````
будет создана строчка из примера запроса, описанного в TASK.md

##Запуск сервиса
- linux
``````bash
./gradlew bootRun
``````
- windows
```
gradlew.bat bootRun
```
После инициализации веб сервис будет доступен по url http://localhost:8080/ws/operatorsSvc.wsdl
```bash
curl http://localhost:8080/ws/operatorsSvc.wsdl
```
для тестирования в chrome можно воспользоваться [Wizdler extension](https://chrome.google.com/webstore/detail/wizdler/oebpmncolmhiapingjaagmapififiakb).
