BEGIN
  IF NOT EXISTS (SELECT * FROM [dbo].[operators]
                  WHERE id_operator = 2222)
  BEGIN
    INSERT INTO [dbo].[operators]
           ([id_operator]
           ,[Family]
           ,[Name]
           ,[Farth]
           ,[snils]
           ,[id_otdel]
           ,[position])
     VALUES
           (2222
           ,'Хвостова'
           ,'Екатерина'
           ,'Юрьевна'
           ,'108-003-636 06'
           ,186
           ,0)
  END
END
