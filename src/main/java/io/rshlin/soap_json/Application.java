package io.rshlin.soap_json;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.io.IOException;
import java.net.URL;

@SpringBootApplication
public class Application {
    private final static Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }

    @Configuration
    @ConditionalOnProperty(prefix = "io.rshlin", name = "testData", havingValue = "init")
    public static class TestDbDataConfiguration {

        private final EntityManager em;

        public TestDbDataConfiguration(EntityManager em) {
            this.em = em;
        }


        @Transactional
        @SuppressWarnings("UnstableApiUsage")
        @EventListener(ApplicationReadyEvent.class)
        public void insertTestData() throws IOException {
            log.info("inserting test rows");
            URL resource = Resources.getResource("test/testRows.sql");
            String sql = Resources.toString(resource, Charsets.UTF_8);

            int count = em.createNativeQuery(sql).executeUpdate();
            log.info("insertion completed. {} rows affected", count);
        }

    }
}
