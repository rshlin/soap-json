package io.rshlin.soap_json.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@ToString
@Entity
@Table(name = "operators", schema = "dbo")
public class OperatorEntity {
    @Id
    @Column(name = "id_operator")
    private Integer idOperator;
    @Column(name = "Family")
    private String family;
    @Column(name = "Name")
    private String name;
    @Column(name = "Farth")
    private String farth;
    private String pass;
    private Short role;
    @Column(name = "dp_FIO")
    private String dpFIO;
    @Column(name = "logname")
    private String logname;
    @Column(name = "log_name")
    private String log_name;
    private String phone;
    @Column(name = "DOB")
    private Date dob;
    private String picture;
    private String passport;
    private String snils;
    @Column(name = "no_visible")
    private Short noVisible;
    @Column(name = "id_otdel")
    private Integer idOtdel;
    @Column(name = "GlobalKey", columnDefinition = "uniqueidentifier")
    private String globalKey;
    private Short position;
    @Column(name = "rabbit_status")
    private Short rabbitStatus;
    @Column(name = "date_last_update")
    private Date dateLastUpdate;
    @Column(name = "Id_Otdel_Sync")
    private Integer idOtdelSync;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OperatorEntity that = (OperatorEntity) o;
        return getIdOperator().equals(that.getIdOperator());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdOperator());
    }
}
