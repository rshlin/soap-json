package io.rshlin.soap_json.service;

import io.rshlin.soap_json.entity.OperatorEntity;
import io.rshlin.soap_json.repository.OperatorRepository;
import io.rshlin.soap_json.repository.OperatorSpecification;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.*;

@Service
public class OperatorService {
    private final OperatorRepository operatorRepository;

    public OperatorService(OperatorRepository operatorRepository) {
        this.operatorRepository = operatorRepository;
    }

    public Collection<OperatorEntity> findByParamsEqual(@NotNull Map<String, Object> params) {
        Specification<OperatorEntity> specification = getAllEqualSpec(params);

        return Optional.ofNullable(specification)
                .map(operatorRepository::findAll)
                .orElse(Collections.emptyList());
    }

    private Specification<OperatorEntity> getAllEqualSpec(@NotNull Map<String, Object> params) {
        Specification<OperatorEntity> specification = null;
        for (Map.Entry<String, Object> param : params.entrySet()) {
            if (!StringUtils.isEmpty(param.getKey())) {
                if (Objects.isNull(specification)) {
                    specification = whereEqual(param);
                } else {
                    specification = specification.and(whereEqual(param));
                }
            }
        }
        return specification;
    }

    private Specification<OperatorEntity> whereEqual(Map.Entry<String, Object> param) {
        return Specification.where(OperatorSpecification.attributeEqualTo(param.getKey(), param.getValue()));
    }
}
