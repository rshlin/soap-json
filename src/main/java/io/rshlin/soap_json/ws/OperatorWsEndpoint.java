package io.rshlin.soap_json.ws;

import io.rshlin.soap_json.entity.OperatorEntity;
import io.rshlin.soap_json.operators.GetOperatorDetailsRequest;
import io.rshlin.soap_json.operators.GetOperatorDetailsResponse;
import io.rshlin.soap_json.operators.ObjectFactory;
import io.rshlin.soap_json.service.OperatorService;
import io.rshlin.soap_json.util.JsonKit;
import io.rshlin.soap_json.ws.dto.OperatorDto;
import io.rshlin.soap_json.ws.dto.OperatorMapper;
import io.rshlin.soap_json.ws.dto.OperatorSearchRs;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.stream.Collectors;

@Endpoint
public class OperatorWsEndpoint {
    private final static Logger log = LoggerFactory.getLogger(OperatorWsEndpoint.class);

    private static final String NAMESPACE_URI = "http://rshlin.io/soap_json/operators";

    private final OperatorService operatorService;

    public OperatorWsEndpoint(OperatorService operatorService) {
        this.operatorService = operatorService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getOperatorDetailsRequest")
    @ResponsePayload
    public GetOperatorDetailsResponse getOperatorDetails(@RequestPayload GetOperatorDetailsRequest request)
        //CompletableFuture API's exceptions are irrelevant since we execute code in the single thread
            throws ExecutionException, InterruptedException {

        CompletableFuture<String> cf = CompletableFuture.completedFuture(request.getRqParams());

        return cf.thenApply(JsonKit::toMap)
                .thenApply(operatorService::findByParamsEqual)
                .thenApply(entities2Dto())
                .thenApply(dto2SearchRs())
                .thenApply(JsonKit::toJson)
                .thenApply(createGetOperatorDetailsResponse())
                //simple error handling for the sake of simplicity
                .exceptionally(handleException())
                .get();
    }

    private Function<Collection<OperatorEntity>, List<OperatorDto>> entities2Dto() {
        return operatorEntities ->
                operatorEntities.stream()
                        .map(OperatorMapper.INSTANCE::entityToDto)
                        .collect(Collectors.toList());
    }

    private Function<List<OperatorDto>, OperatorSearchRs> dto2SearchRs() {
        return dtos ->
                OperatorSearchRs.builder()
                        .success(Boolean.TRUE)
                        .description("Операция выполнена успешно")
                        .operators(dtos)
                        .build();
    }

    private Function<String, GetOperatorDetailsResponse> createGetOperatorDetailsResponse() {
        return json -> {
            GetOperatorDetailsResponse rs = new ObjectFactory().createGetOperatorDetailsResponse();
            rs.setRs(json);
            return rs;
        };
    }

    private Function<Throwable, GetOperatorDetailsResponse> handleException() {
        return throwable -> {
            log.warn("failed to process operators search query", throwable);
            OperatorSearchRs failedRs = OperatorSearchRs.builder()
                    .success(Boolean.FALSE)
                    .description(String.format("Операция не выполнена. Ошибка:\n%s", ExceptionUtils.getStackTrace(throwable)))
                    .build();
            return createGetOperatorDetailsResponse().apply(JsonKit.toJson(failedRs));
        };
    }
}
