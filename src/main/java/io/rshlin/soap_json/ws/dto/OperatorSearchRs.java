package io.rshlin.soap_json.ws.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Collection;

@Data
@Builder
public class OperatorSearchRs {
    private Boolean success;
    private String description;
    private Collection<OperatorDto> operators;
}
