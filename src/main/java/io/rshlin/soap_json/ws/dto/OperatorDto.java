package io.rshlin.soap_json.ws.dto;

import lombok.Data;

@Data
public class OperatorDto {
    private String id;
    private String surname;
    private String name;
    private String patronymic;
    private String position;
    private String branch;
}
