package io.rshlin.soap_json.ws.dto;

import io.rshlin.soap_json.entity.OperatorEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.HashMap;
import java.util.Map;

@Mapper
public interface OperatorMapper {
    OperatorMapper INSTANCE = Mappers.getMapper(OperatorMapper.class);
    Map<Integer, String> BRANCH_NAMES = new HashMap<Integer, String>() {{
        put(186, "Уполномоченный МФЦ");
    }};
    Map<Short, String> POSITION_NAMES = new HashMap<Short, String>() {{
        put((short) 0, "Оператор");
    }};

    @Mappings({
            @Mapping(source = "idOperator", target = "id"),
            @Mapping(source = "family", target = "surname"),
            @Mapping(source = "farth", target = "patronymic"),
            @Mapping(expression = "java(io.rshlin.soap_json.ws.dto.OperatorMapper.BRANCH_NAMES.getOrDefault(entity.getIdOtdel(), \"Неизвестно\"))", target = "branch"),
            @Mapping(expression = "java(io.rshlin.soap_json.ws.dto.OperatorMapper.POSITION_NAMES.getOrDefault(entity.getPosition(), \"Неизвестно\"))", target = "position")
    })
    OperatorDto entityToDto(OperatorEntity entity);
}
