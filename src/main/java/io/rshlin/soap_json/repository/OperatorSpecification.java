package io.rshlin.soap_json.repository;

import io.rshlin.soap_json.entity.OperatorEntity;
import org.springframework.data.jpa.domain.Specification;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class OperatorSpecification {
    public static Specification<OperatorEntity> attributeEqualTo(@NotEmpty(message = "attribute name can not be empty") final String attributeName, @NotNull(message = "attribute value can not be null") final Object attributeValue) {
        return (Specification<OperatorEntity>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(attributeName), attributeValue);
    }
}
