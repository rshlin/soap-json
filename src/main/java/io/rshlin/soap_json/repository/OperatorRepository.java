package io.rshlin.soap_json.repository;

import io.rshlin.soap_json.entity.OperatorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface OperatorRepository extends JpaRepository<OperatorEntity, Integer>, JpaSpecificationExecutor<OperatorEntity> {
}
